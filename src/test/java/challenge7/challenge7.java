package challenge7;

import resources.base.BaseTest;
import resources.AdditionalConditions.AdditionalConditions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.*;
import java.util.List;

public class challenge7 extends BaseTest {

    @Test
    public void challenge7() {
        /* Test case solving challenge 7 */
        driver.get("https://www.copart.com");
        Assert.assertTrue(driver.getTitle().contains("Copart"));
        String makeModelTableXP = "//*[@ng-if='popularSearches']//a";
        List<WebElement> popularItems = driver.findElements(By.xpath(makeModelTableXP));
        Map<String, String> makeModelMap = new HashMap<>();
        for (WebElement item : popularItems) {
            makeModelMap.put(item.getText(), item.getAttribute("href"));
        }
        System.out.println(makeModelMap);
        ArrayList<String> failures = new ArrayList<String>();
        for ( Map.Entry<String, String> entry : makeModelMap.entrySet() ) {
            String key = entry.getKey();
            String value = entry.getValue();
            driver.get(value);
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(AdditionalConditions.angularHasFinishedProcessing());
            String vehTableXP = "//*[@id='serverSideDataTable']";
            if (!wait.until(ExpectedConditions.textToBePresentInElement((WebElement) driver.findElement(By.xpath(vehTableXP)),key))) {
                failures.add("There was an issue navigating to " + value + " and ensuring the page contained " + key);
            }
        }
        Assert.assertEquals(failures.size(),0);
    }
}
