package challenge9;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import org.testng.annotations.Test;
import resources.API.APIHelper;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.util.*;

public class challenge9 {

    @Test
    public challenge9() throws IOException {
        /* Test case solving challenge 9 */
        ArrayList<String> expectedStrList = new ArrayList<>();
        ArrayList<String> expectedIntList = new ArrayList<>();
        ArrayList<String>  expectedBoolList = new ArrayList<>();
        ArrayList<String>  expectedLongList = new ArrayList<>();
        ArrayList<String> expectedDoubleList = new ArrayList<>();
        ArrayList<String> expectedArrayList = new ArrayList<>();
        ArrayList<String>  failures = new ArrayList<>();
        String vehicle = "toyota camry";

        APIHelper temp = new APIHelper();
        HttpsURLConnection conn = temp.establishCopartConnection();

        JsonObject obj = temp.postRequest(conn, vehicle);
        JsonObject temp1 = (JsonObject) obj.get("data");
        JsonObject temp2 = (JsonObject) temp1.get("results");
        HashMap<String, Object> results = new ObjectMapper().readValue(String.valueOf(temp2), new TypeReference<Map<String, Object>>() {});

        ArrayList tem = (ArrayList) results.get("content");

        Map<String, Object> map = (HashMap) tem.get(0);

        for (Map.Entry<String, Object> e : map.entrySet()) {
            if (e.getValue() instanceof Integer) {
                expectedIntList.add(e.getKey());
            } else if (e.getValue() instanceof String) {
                expectedStrList.add(e.getKey());
            } else if (e.getValue() instanceof Long) {
                expectedLongList.add(e.getKey());
            } else if (e.getValue() instanceof Double) {
                expectedDoubleList.add(e.getKey());
            } else if (e.getValue() instanceof Boolean) {
                expectedBoolList.add(e.getKey());
            } else if (e.getValue() instanceof ArrayList) {
                expectedArrayList.add(e.getKey());
            } else {
                System.out.println(e.getValue() + "UNEXPECTED TYPE");
                failures.add((String) e.getValue());
            }
        }
        System.out.println("String List: " + expectedStrList + "\nInteger List: " + expectedIntList +
                "\nBoolean List: " + expectedBoolList + "\nLong List: " + expectedLongList + "\nDouble List" +
                expectedDoubleList + "\nArray List:" + expectedArrayList + "\nUnexpected Types: " + failures);
    }
}
