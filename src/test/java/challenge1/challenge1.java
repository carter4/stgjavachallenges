package challenge1;

import resources.base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.*;

/**
 *  This class contains the challenges for the Associate portion of the challenges
 *  located here: https://bitly.com/STGjavawebdriver
 */
public class challenge1 extends BaseTest {

    @Test()
    public void challenge1() {
        /* Test case solving challenge 1 */
        driver.get("https://www.google.com");
        Assert.assertEquals(driver.getTitle(), "Google");
    }
}
