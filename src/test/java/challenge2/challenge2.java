package challenge2;

import resources.CopartHomePage.CopartHomePage;
import resources.base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class challenge2 extends BaseTest {

    @Test()
    public void challenge2() {
        /* Test case solving challenge 2 */
        driver.get("https://www.copart.com");
        Assert.assertTrue(driver.getTitle().contains("Copart"));
        CopartHomePage temp = new CopartHomePage();
        temp.searchTextbox(driver, "exotics", "PORSCHE");
    }
}
