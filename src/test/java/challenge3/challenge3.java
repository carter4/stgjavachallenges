package challenge3;

import resources.base.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class challenge3 extends BaseTest {

    @Test()
    public void challenge3() {
        /* Test case solving challenge 3 */
        driver.get("https://www.copart.com");
        Assert.assertTrue(driver.getTitle().contains("Copart"));
        String makeModelTableXP = "//*[@ng-if='popularSearches']//a";
        List<WebElement> popularItems = driver.findElements(By.xpath(makeModelTableXP));
        for (WebElement item : popularItems) {
            System.out.println(item.getText() + " - " + item.getAttribute("href"));
        }
    }
}
