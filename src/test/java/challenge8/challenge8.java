package challenge8;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.testng.annotations.Test;
import resources.TextFiles.WriteFile;
import resources.API.APIHelper;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class challenge8 {

    @Test
    public challenge8() {
        /* Test case solving challenge 8 */
        List<String> vehicleQueries = Arrays.asList("toyota camry", "nissan skyline", "subaru forester", "subaru crosstek",
                "chevy silverado", "dodge cummins", "ford f150", "toyota sienna", "honda accord", "honda crv");

        for (String vehicle : vehicleQueries){
        try {
            APIHelper temp = new APIHelper();
            HttpsURLConnection conn = temp.establishCopartConnection();

            JsonObject obj = temp.postRequest(conn, vehicle);

            JsonObject temp1 = (JsonObject) obj.get("data");
            JsonObject temp2 = (JsonObject) temp1.get("results");
            JsonElement totalElements = temp2.get("totalElements");

            WriteFile data = new WriteFile("src/testResults/log.txt" , true );
            data.writeToFile(vehicle + ": " + totalElements);
            System.out.println("Wrote " + vehicle + ": " + totalElements + " to log");
            conn.disconnect();
        } catch(IOException e) {
            e.printStackTrace();
        }
        }
    }
}
