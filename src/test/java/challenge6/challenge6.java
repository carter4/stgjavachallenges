package challenge6;

import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import resources.CopartHomePage.CopartHomePage;
import resources.base.BaseTest;
import resources.Screenshot.Screenshot;
import java.io.IOException;

public class challenge6 extends BaseTest {


    @Test
    public void challenge6() throws IOException {
        /* Test case solving challenge 6 */
        driver.get("https://www.copart.com");
        Assert.assertTrue(driver.getTitle().contains("Copart"));
        CopartHomePage temp = new CopartHomePage();
        temp.searchTextbox(driver, "Nissan", "NISSAN");
        String modelDropDownXP = "//*[@data-uname='ModelFilter']";
        driver.findElement(By.xpath(modelDropDownXP)).click();
        String modelTextboxXP = "//*[@id='collapseinside4']/form/div/input"; // tried to simplify this path, no luck yet
        driver.findElement(By.xpath(modelTextboxXP)).sendKeys("Skyline1");
        try {
            // using skyline1 to intentionally break test
            String modelSkylineOptionXP = "//*[@id='lot_model_descSKYLINE1']";
            driver.findElement(By.xpath(modelSkylineOptionXP)).click();
        }
        catch (Exception e){
            Screenshot snapshot = new Screenshot();
            String screenshotPath = snapshot.captureScreen(driver, "challenge6");
            System.out.println(screenshotPath);
        }
    }
}
