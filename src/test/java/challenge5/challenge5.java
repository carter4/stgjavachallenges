package challenge5;

import resources.CopartHomePage.CopartHomePage;
import resources.TableParser.TableParser;
import resources.base.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.ArrayList;

import java.util.HashSet;
import java.util.List;

public class challenge5 extends BaseTest {

    @Test
    public void challenge5() {
        /* Test case solving challenge 5 */
        driver.get("https://www.copart.com");
        Assert.assertTrue(driver.getTitle().contains("Copart"));
        CopartHomePage temp = new CopartHomePage();
        WebDriverWait wait = temp.searchTextbox(driver, "PORSCHE", "PORSCHE");
        String entriesDD100 = "//*[@id='serverSideDataTable_length']//option[3]";
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(entriesDD100)));
        driver.findElement(By.xpath(entriesDD100)).click();
        // get list of models, determine count of each model
        String vehTableXP = "//*[@id='serverSideDataTable']";
        wait.until(ExpectedConditions.textToBePresentInElement((WebElement) driver.findElement(By.xpath(vehTableXP)),"PORSCHE"));
        TableParser modelColumnValues = new TableParser();
        List allModels = modelColumnValues.tableParser(driver, "serverSideDataTable", "Model");
        HashSet<String> uniqueModels = new HashSet<>(allModels);
        for (Object uniqueModel : uniqueModels) {
            ArrayList<Object> countOfModel = new ArrayList<>();
            for (Object allModel : allModels) {
                if (allModel.equals(uniqueModel)) {
                    countOfModel.add(uniqueModel);
                }
            }
            System.out.println("The number of " + uniqueModel + " models on the page is " + countOfModel.size());
            countOfModel.clear();
        }
        // end of count models. begin damage data collection process
        TableParser damagesColumnValues = new TableParser();
        List allDamages = damagesColumnValues.tableParser(driver, "serverSideDataTable", "Damage");
        int rearEndCount = 0;
        int frontEndCount = 0;
        int minorDentScratchCount = 0;
        int undercarriageCount = 0;
        int miscCount = 0;
        for (Object damage : allDamages){
            switch ((String) damage){
                case "REAR END":
                    rearEndCount++;
                    break;
                case "FRONT END":
                    frontEndCount++;
                    break;
                case "MINOR DENT/SCRATCHES":
                    minorDentScratchCount++;
                    break;
                case "UNDERCARRIAGE":
                    undercarriageCount++;
                    break;
                default:
                    miscCount++;
            }
        }
        System.out.println("The number of vehicles with REAR END damage type is " + rearEndCount);
        System.out.println("The number of vehicles with FRONT END damage type is " + frontEndCount);
        System.out.println("The number of vehicles with MINOR DENT/SCRATCHES damage type is " + minorDentScratchCount);
        System.out.println("The number of vehicles with UNDERCARRIAGE damage type is " + undercarriageCount);
        System.out.println("The number of vehicles with other misc damage: " + miscCount);

    }
}