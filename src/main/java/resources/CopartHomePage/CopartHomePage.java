package resources.CopartHomePage;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import resources.AdditionalConditions.AdditionalConditions;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class CopartHomePage {


    public WebDriverWait searchTextbox(WebDriver driver, String searchText, String expectedText) {
        String textboxXP = "//*[@id='input-search']";
        driver.findElement(By.xpath(textboxXP)).sendKeys(searchText, Keys.RETURN);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(AdditionalConditions.angularHasFinishedProcessing());
        String vehTableXP = "//*[@id='serverSideDataTable']";
        wait.until(ExpectedConditions.textToBePresentInElement((WebElement) driver.findElement(By.xpath(vehTableXP)),expectedText));
        return wait;
    }

    public static String getIncapsulaCookie(String url) {

        String USER_AGENT = "Mozilla/5.0";
        BufferedReader in = null;

        String incapsulaCookie = null;

        try {

            HttpsURLConnection cookieConnection =
                    (HttpsURLConnection) new URL(url).openConnection();
            cookieConnection.setRequestMethod("GET");
            cookieConnection.setRequestProperty("Accept",
                    "text/html; charset=UTF-8");
            cookieConnection.setRequestProperty("User-Agent", USER_AGENT);

            // Disable 'keep-alive'
            cookieConnection.setRequestProperty("Connection", "close");

            // Cookies for Incapsula, preserve order
            String visid = null;
            String incap = null;

            cookieConnection.connect();

            for (Map.Entry<String, List<String>> header : cookieConnection
                    .getHeaderFields().entrySet()) {

                // Incapsula gives you the required cookies
                if (header.getKey() != null
                        && header.getKey().equals("Set-Cookie")) {

                    // Search for the desired cookies
                    for (String cookieValue : header.getValue()) {
                        if (cookieValue.contains("visid")) {
                            visid = cookieValue.substring(0,
                                    cookieValue.indexOf(";") + 1);
                        }
                        if (cookieValue.contains("incap_ses")) {
                            incap = cookieValue.substring(0,
                                    cookieValue.indexOf(";"));
                        }
                    }
                }
            }

            incapsulaCookie = visid + " " + incap;

            // Explicitly disconnect, also essential in this method!
            cookieConnection.disconnect();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null)
                    in.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return incapsulaCookie;
    }

}
