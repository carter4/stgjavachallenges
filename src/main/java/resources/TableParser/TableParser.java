package resources.TableParser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

public class TableParser {

    public List<String> tableParser(WebDriver driver, String tableID, String columnName) {

        List<List<String>> trowtdata = new ArrayList<>();
        int columnIndexValue = 0;
        Document doc = Jsoup.parse(driver.getPageSource());
        Element table = doc.getElementById(tableID);
        Elements trs = table.select("tr");
        for (int i = 0; i < trs.size(); i++) {
            if (i == 0) {
                Elements tds = trs.get(i).select("th");
                trowtdata.add(new ArrayList<>());

                for (int j = 0; j < tds.size(); j++) {
                    trowtdata.get(i).add(tds.get(j).text());
                    if (trowtdata.get(i).get(j).equals(columnName)) columnIndexValue = j;
                }
            } else {
                Elements tds = trs.get(i).select("td");
                trowtdata.add(new ArrayList<>());

                for (Element td : tds)
                    trowtdata.get(i).add(td.text());
            }
        }
        List<String> commonValues = new ArrayList<>();
        for (List<String> rowList : trowtdata)
            commonValues.add(rowList.get(columnIndexValue));

        return commonValues;
    }
}
