package resources.API;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import resources.CopartHomePage.CopartHomePage;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class APIHelper {

    public HttpsURLConnection establishCopartConnection() throws IOException{
        URL searchURL = new URL("https://www.copart.com/public/lots/search/");
        String homeURL = "https://www.copart.com";
        String incapsulaCookie = CopartHomePage.getIncapsulaCookie(homeURL);
        HttpsURLConnection conn = (HttpsURLConnection) searchURL.openConnection();
        conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36");
        conn.setRequestProperty("Cookie", incapsulaCookie);
        return conn;
    }

    public JsonObject postRequest(HttpsURLConnection conn, String vehicle) throws IOException {

        conn.setRequestMethod("POST");
        conn.setRequestProperty("Query", vehicle);
        String urlParameters = "";
        conn.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return new JsonParser().parse(String.valueOf(response)).getAsJsonObject();
    }
}
