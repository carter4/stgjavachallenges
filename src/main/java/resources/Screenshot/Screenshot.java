package resources.Screenshot;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;
import resources.OSDetector.OSDetector;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Screenshot {
    public String captureScreen(WebDriver driver, String testName) {

        String filePath;
        String filePathWin = "src\\testResults\\screenshots\\";
        String filePathMac = "src/testResults/screenshots/";
        String fileType = ".png";
        try {
            OSDetector osDetect = new OSDetector();

            Date now = new Date();
            String date = new SimpleDateFormat("MMM_d_yyyy").format(now);
            String time = new SimpleDateFormat("HH_mm_ss_SSS").format(now);
            TakesScreenshot snapper;
            if  (((RemoteWebDriver) driver).getCapabilities().getCapability("webdriver.remote.sessionid") == null) snapper = (TakesScreenshot)driver;
            else  {
                Augmenter augmenter = new Augmenter();
                snapper = (TakesScreenshot) augmenter.augment(driver);
            }
            File tempScreenshot = snapper.getScreenshotAs(OutputType.FILE);
            String os = osDetect.detectOS();
            if (os.equals("Windows")) {
                filePath = filePathWin;
            } else {
                filePath = filePathMac;
            }

            File myScreenshotDirectory = new File(filePath + date + "/");

            if (!myScreenshotDirectory.exists()){
                if (myScreenshotDirectory.mkdirs())
                System.out.println("Directory for screenshot created at " + myScreenshotDirectory);
            }
            File myScreenshot = new File(myScreenshotDirectory, time + " " + testName + fileType);
            FileUtils.moveFile(tempScreenshot, myScreenshot);
            return myScreenshot.getAbsolutePath();

        }
        catch(IOException e) {
            return e.getMessage();
        }
    }
}
