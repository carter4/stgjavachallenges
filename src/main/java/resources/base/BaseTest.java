package resources.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;

public class BaseTest {

    protected WebDriver driver;

    @AfterSuite
    public void stopSuite() throws Exception {
        System.out.println("All done!!!");
    }

    @BeforeClass
    public WebDriver startClass() throws Exception {
        String browserSelection = "chrome"; // UPDATE BROWSER CHOICE HERE
        if (browserSelection.equals("chrome")) {
            System.setProperty("webdriver.chrome.driver", "bin/chromedriver");
            driver = new ChromeDriver();
        } else if (browserSelection.equals("firefox")){
            System.setProperty("webdriver.gecko.driver", "bin/geckodriver");
            driver =  new FirefoxDriver();
        }
        else
            System.out.println("Please select a valid browser");
        driver.manage().window().maximize();
        return driver;
    }

    @AfterClass
    public void stopClass(){
        driver.quit();
    }
}